// import {Fragment} from 'react';
import {useState} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Hightlights from './components/Highlights';
import Courses from './pages/Courses'
import Error from './pages/Error'
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

    // This state hook is for the user state that's defined here for the a global.
    const [user, setUser] = useState({token: localStorage.getItem("token")});

    // Function for clearing the localStorage to logout the user.
    const unsetUser = () => {
        localStorage.clear();
    }



  return (

    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <Container fluid>
                <AppNavbar/>
                <Routes>
                   <Route path="/" element={<Home/>} />
                   <Route path="/courses" element={<Courses/>} />
                   <Route path="/register" element={<Register/>} />
                   <Route path="/login" element={<Login/>} />
                   <Route path="/logout" element={<Logout/>} />
                   <Route path="/*" element={<Error/>} />
                </Routes>
            </Container>
        </Router>
    </UserProvider>

  );
}

export default App;
