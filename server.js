// Express Setup

// 1. Import express using the 'require' directive to get access to the components of express package/dependency.
const express = require('express');

// 2. Use express() function and assign it to the "app" variable to create an express app or server.
const app = express();

// 3. Define the port number in a port variable.
const port = 3000;


// 4. Use the middlewares to enable our server to read JSON as regular JavaScript and read data from forms.
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// 5. You can have your routes after setting up the server.

// ROUTES
// Express has methods corresponding to each HTTP methods.
// The full base URI of our local application for this route will be at "http://localhost:3000/"


// Get request route
app.get('/', (req, res) => {
	// Once the route "/" is accesses it will then send a string response saying "Hello World!".
	res.send('Hello World!');
});

// Post request route
// Uses the POST http method in processing the request and sending back the response.
// We can have similar routes, given that these use different HTTP methods.

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
});

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Register user route

// Mock data base.
let users = [{
    "userId": 1,
    "username": "john_doe",
    "password": "password123",
    "name": "John Doe",
    "age": 30,
    "email": "john.doe@example.com",
    "country": "USA"
  },
  {
    "userId": 2,
    "username": "jane_smith",
    "password": "pass123",
    "name": "Jane Smith",
    "age": 28,
    "email": "jane.smith@example.com",
    "country": "Canada"
  },
  {
    "userId": 3,
    "username": "mike_johnson",
    "password": "testpassword",
    "name": "Mike Johnson",
    "age": 35,
    "email": "mike.johnson@example.com",
    "country": "UK"
  },
  {
    "userId": 4,
    "username": "emma_williams",
    "password": "abc123",
    "name": "Emma Williams",
    "age": 25,
    "email": "emma.williams@example.com",
    "country": "Australia"
  },
  {
    "userId": 5,
    "username": "alex_smith",
    "password": "securepass",
    "name": "Alex Smith",
    "age": 24,
    "email": "alex.smith@example.com",
    "country": "USA"
  },
  {
    "userId": 6,
    "username": "sophie_brown",
    "password": "userpass",
    "name": "Sophie Brown",
    "age": 31,
    "email": "sophie.brown@example.com",
    "country": "Canada"
  },
  {
    "userId": 7,
    "username": "peter_jackson",
    "password": "pass1234",
    "name": "Peter Jackson",
    "age": 27,
    "email": "peter.jackson@example.com",
    "country": "UK"
  },
  {
    "userId": 8,
    "username": "olivia_smith",
    "password": "oliviapass",
    "name": "Olivia Smith",
    "age": 29,
    "email": "olivia.smith@example.com",
    "country": "Australia"
  },
  {
    "userId": 9,
    "username": "david_wilson",
    "password": "davidpass",
    "name": "David Wilson",
    "age": 33,
    "email": "david.wilson@example.com",
    "country": "USA"
  },
  {
    "userId": 10,
    "username": "sara_taylor",
    "password": "sarataylor",
    "name": "Sara Taylor",
    "age": 26,
    "email": "sara.taylor@example.com",
    "country": "Canada"
  }];

app.post('/register', (req, res) => {
	console.log(req.body)

	if(req.body.username !== " " && req.body.password !== " ") {
		users.push(req.body)
		console.log(users)
		res.send(`User ${req.body.username}, successfully registered!`)
	} else {
		res.send('Please input BOTH username and password.')
	}
});

// Put Request route

app.put('/change-password', (req, res) => {
	let message;

	for(let i = 0; i <users.length; i++) {
		// If the username from the reqBody matches the username from users mock DB, change the password of the user.
		if(req.body.username == users[i].username){
			users[i].password == req.body.password
			message = `User ${req.body.username}'s password has been updated.`
			console.log(users);
			break;
		} else{
			// If no user matches, set the message var to a string saying that the user does not exist.
			message = `User does not exist.`
		}
	}

	// Send back a response to the client once the password has been updated or if the user is not found.

	res.send(message);
});



// S40 Activity Code Here:

// Get request at "/home" route

// This route expects to receive a GET request at the URI "/home"
app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
})


// Get request at "/users" route

// This route expects to receive a GET request at the URI "/users"
// This will retrieve all the users stored in the variable created above
app.get("/users", (req, res) => {
    res.send(users);
})

// This route expects to receive a DELETE request at the URI "/delete-user"
// This will a user from the array for deletion
app.delete("/delete-user", (req, res) => {

    // Creates a variable to store the message to be sent back to the client/Postman 
    let message;

    // Creates a condition if there are users found in the array
    if (users.length != 0){

        // Creates a for loop that will loop through the elements of the "users" array
        for(let i = 0; i < users.length; i++){

            // If the username provided in the client/Postman and the username of the current object in the loop is the same
            if (req.body.username == users[i].username) {

                // The splice method manipulates the array and removes the user object from the "users" array based on it's index
                // users[i] is used here to indicate the start of the index number in the array for the element to be removed
                // The number 1 defines the number of elements to be removed from the array
                users.splice(users[i], 1);
                // Changes the message to be sent back by the response
                message = `User ${req.body.username} has been deleted.`;

                break;

            } 

        }

    if(message === undefined){
            message = "User does not exist."
     }

    // If no user was found
    } else {
        // Changes the message to be sent back by the response
        message = "No users found.";

    }
    
    // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
    res.send(message);

})









// Delete request route











// app.listen(port, () => console.log(`Server is running at port: ${port}.`))

//An if statement is added here to be able to export the following app/server for checking.
if(require.main === module){
    app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;

